import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.styl']
})
export class AppComponent implements OnInit{
  title = 'Bike-appA9';

  numberOne: number = 56;
  numberTwo: number = 10;
  result1: number;
  result2: number;
  result3: number;
  result4: number;

  constructor() {}

  ngOnInit(): void{
    this.sumar();
    this.restar();
    this.multiplicar();
    this.dividir();
  }

  private sumar() {
    this.result1 = this.numberOne + this.numberTwo;
    console.log('SUMA-RESULT: ', this.result1);
  }
  private restar() {
    this.result2 = this.numberOne - this.numberTwo;
    console.log('RESTA-RESULT: ', this.result2);
  }
  private multiplicar() {
    this.result3 = this.numberOne * this.numberTwo;
    console.log('MULTIPLICACIÓN-RESULT: ', this.result3);
  }
  private dividir() {
    this.result4 = this.numberOne / this.numberTwo;
    console.log('DIVISIÓN-RESULT: ', this.result4);
  }
}

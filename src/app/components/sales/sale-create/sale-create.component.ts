import { Component, OnInit } from '@angular/core';
import { Bike } from '../../bikes/interfaces/bike';
import { SalesService } from '../sales.service';
import { BikesService } from '../../bikes/bikes.service';
import { ISale } from '../interfaces/sale';
import { ClientsService } from '../../clients/clients.service';
import { Client } from '../../clients/interfaces/clients';
import { FormBuilder, FormGroup } from '@angular/forms';

@Component({
  selector: 'app-sale-create',
  templateUrl: './sale-create.component.html',
  styleUrls: ['./sale-create.component.styl']
})
export class SaleCreateComponent implements OnInit {

  bikesList: Bike[];
  clientsList: Client[];
  salesList: ISale[] = [];
  clientList: Client[] = [];
  clientSelectedTemp: Client;
  total: number = 0;
  
  searchForm = this.fb.group({
    document: ''
  });

  constructor(private salesService: SalesService,
    private bikesService: BikesService,
    private clientsService: ClientsService,
    private fb: FormBuilder
    ) { }

  ngOnInit(): void {
    this.bikesService.query()
      .subscribe(res => {
        this.bikesList = res;
      });
    /*
    this.clientsService.query()
      .subscribe(res => {
        this.clientsList = res;
      });
    */
  }

  

  addClient(client: Client): void {
    this.clientSelectedTemp = client;
    console.warn('Selected client', this.clientSelectedTemp);
    /* 
    console.warn('Selected', client);
    this.clientList.push({
      id: client.id,
      name: client.name,
      document: client.document,
      email: client.email,
      phoneNumber: client.phoneNumber,
      documentType: client.documentType,
    });
    */
  }

  buyItem(bike: Bike): void {
    this.total += bike.price;
    console.warn('Selected', bike);
    this.salesList.push({
      clientId: this.clientSelectedTemp.id,
      clientName:this.clientSelectedTemp.name,
      bikeId: bike.id,
      bikeSerial: bike.serial,
      bike: bike,
    });
    /*//sumar el valor de las bicicletas
    this.total = this.salesList.reduce((
      monto1,
      monto2,
    ) => monto1 + monto2.bike.price,0);
    console.warn('total', this.total)
    */
  }

  searchClient(): void {
    console.warn('Key Up');
    this.clientsService.query({
      'document.contains': this.searchForm.value.document
    })
    .subscribe(res => {
      this.clientList = res;
    })
  }

}








import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SaleCreateComponent } from './sale-create/sale-create.component';
import { SalesRoutingModule } from './sales-routing.module';
import { ReactiveFormsModule } from '@angular/forms';
import { SaleListComponent } from './sale-list/sale-list.component';
import { MainSalesComponent } from './main-sales/main-sales.component';



@NgModule({
  declarations: [
    SaleCreateComponent, 
    SaleListComponent, MainSalesComponent
  ],
  imports: [
    CommonModule, 
    SalesRoutingModule,
    ReactiveFormsModule
  ]
})
export class SalesModule { }

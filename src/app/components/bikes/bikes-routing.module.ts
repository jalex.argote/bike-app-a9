import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { BikesListComponent } from './bikes-list/bikes-list.component';
import { BikesCreateComponent } from './bikes-create/bikes-create.component';
import { BikesUpdateComponent } from './bikes-update/bikes-update.component';
import { BokesViewComponent } from './bokes-view/bokes-view.component';
import { MainBikesComponent } from './main-bikes/main-bikes.component';


const routes: Routes = [
  {
    path:'',
    component: MainBikesComponent,
    children:[
      {
        path: '',
        component: BikesListComponent
        
      },
      {
        path: 'bikes-create',
        component: BikesCreateComponent
        
      },
      {
        path: 'bikes-update/:id',
        component: BikesUpdateComponent
        
      },
      {
        path: 'bokes-view',
        component: BokesViewComponent
        
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class BikesRoutingModule { }

import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MainBikesComponent } from './main-bikes.component';

describe('MainBikesComponent', () => {
  let component: MainBikesComponent;
  let fixture: ComponentFixture<MainBikesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MainBikesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MainBikesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BikesListComponent } from './bikes-list/bikes-list.component';
import { BikesCreateComponent } from './bikes-create/bikes-create.component';
import { BikesUpdateComponent } from './bikes-update/bikes-update.component';
import { BokesViewComponent } from './bokes-view/bokes-view.component';
import { BikesRoutingModule } from './bikes-routing.module';
import { ReactiveFormsModule } from '@angular/forms';
import { MainBikesComponent } from './main-bikes/main-bikes.component';
import { InputSwitchModule } from 'primeng/inputswitch';
import { FontAwesomeModule, FaIconLibrary } from '@fortawesome/angular-fontawesome';
import { faBicycle, faList, faPencilAlt } from '@fortawesome/free-solid-svg-icons';



@NgModule({
  declarations: [
    BikesListComponent,
    BikesCreateComponent,
    BikesUpdateComponent,
    BokesViewComponent,
    BokesViewComponent,
    MainBikesComponent,

  ],
  imports: [
    CommonModule,
    BikesRoutingModule,
    ReactiveFormsModule,
    InputSwitchModule,
    FontAwesomeModule

  ]
})
export class BikesModule {
  constructor(Library: FaIconLibrary) {
    Library.addIcons(faBicycle, faList, faPencilAlt);
  }
}

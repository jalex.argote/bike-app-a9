import { Component, OnInit } from '@angular/core';
import { Bike } from '../interfaces/bike';
import { BikesService } from '../bikes.service';
import { error } from '@angular/compiler/src/util';


@Component({
  selector: 'app-bikes-list',
  templateUrl: './bikes-list.component.html',
  styleUrls: ['./bikes-list.component.styl']
})
export class BikesListComponent implements OnInit {


  public bikesList: Bike[];
  /*
  pageSize = 5;
  pageNumber = 0;

  listPages = []; // list page*/

  constructor(private bikesServer: BikesService) { }

  ngOnInit() {
    this.bikesServer.query()
      .subscribe(res => {
        this.bikesList = res;
        console.log('Dato Actualizado', res)
      },
        error => console.error('Error', error)
      );
  }

  //ngOnInit pagination
  /*ngOnInit() {
    this.initPagination(this.pageNumber);
  }

  initPagination(page: number): void {
    this.bikesServer.query({
      'pageSize': this.pageSize,
      'pageNumber': page
    })
      .subscribe((res: any) => {
        console.log('DATOS', res);
        this.bikesList = res.content;
        this.formatPage(res.totalPages);
      });
  }
*/
  deleteItem(id: string) {
    console.warn('ID', id);
    this.bikesServer.deleteItem(id)
      .subscribe(res => {
        console.warn('Item Deletec ok..');
        this.ngOnInit();
      }, error => console.warn('Error', error));
  }

  /* //PAGINACIÓN
  private formatPage(countPage: number): void{
    this.listPages = [];
    for(let i = 0; i < countPage; i++){
      this.listPages.push(i);
    }
  }
*/
}

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PostsCreateComponent } from './posts-create/posts-create.component';
import { PostsListComponent } from './posts-list/posts-list.component';
import { PostsRoutingModule } from './posts-routing.module';
import { ReactiveFormsModule } from '@angular/forms';
import { MainPostsComponent } from './main-posts/main-posts.component';



@NgModule({
  declarations: [
    PostsCreateComponent, 
    PostsListComponent, 
    MainPostsComponent
  ],
  imports: [
    CommonModule,
    PostsRoutingModule,
    ReactiveFormsModule
  ]
})
export class PostsModule { }

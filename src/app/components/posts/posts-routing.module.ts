import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PostsCreateComponent } from './posts-create/posts-create.component';
import { PostsListComponent } from './posts-list/posts-list.component';
import { MainPostsComponent } from './main-posts/main-posts.component';


const routes: Routes = [
  {
    path:'',
    component: MainPostsComponent,
    children:[
      {
        path: '',
        component: PostsListComponent
      },
      {
        path: 'posts-create',
        component: PostsCreateComponent
      }
      
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PostsRoutingModule { }

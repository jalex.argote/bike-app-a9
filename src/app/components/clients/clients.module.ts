import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ClientsListComponent } from './clients-list/clients-list.component';
import { ClientsRoutingModule } from './clients-routing.module';
import { ClientsCreateComponent } from './clients-create/clients-create.component';
import { ReactiveFormsModule } from '@angular/forms';
import { ClientsUpdateComponent } from './clients-update/clients-update.component';
import { MainClientsComponent } from './main-clients/main-clients.component';



@NgModule({
  declarations: [
    ClientsListComponent, 
    ClientsCreateComponent, 
    ClientsUpdateComponent, 
    MainClientsComponent
  ],
  imports: [
    CommonModule,
    ClientsRoutingModule,
    ReactiveFormsModule
  ]
})
export class ClientsModule { }

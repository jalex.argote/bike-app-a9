import { Component, OnInit } from '@angular/core';
import { Client } from '../interfaces/clients';
import { ClientsService } from '../clients.service';

@Component({
  selector: 'app-clients-list',
  templateUrl: './clients-list.component.html',
  styleUrls: ['./clients-list.component.styl']
})
export class ClientsListComponent implements OnInit {

  public clientsList: Client [];
  constructor(private clientsServer: ClientsService) { }

  ngOnInit() {
    this.clientsServer.query()
    .subscribe(res => {
      this.clientsList = res;
      console.log('Response Date', res)
    },
    error => console.error('Error', error)
    );
  }

  deleteItem(id: string){
    console.warn('ID', id);
    this.clientsServer.deleteItem(id)
    .subscribe(res => {
      console.warn('Item Deletec ok..');
      this.ngOnInit();
    }, error => console.warn('Error', error));
  }

}

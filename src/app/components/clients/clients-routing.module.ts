import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ClientsListComponent } from './clients-list/clients-list.component';
import { ClientsCreateComponent } from './clients-create/clients-create.component';
import { ClientsUpdateComponent } from './clients-update/clients-update.component';
import { MainClientsComponent } from './main-clients/main-clients.component';


const routes: Routes = [
 {
   path:'',
   component: MainClientsComponent,
   children: [
    {
      path: '',
      component: ClientsListComponent
    },
    {
      path: 'clients-create',
      component: ClientsCreateComponent
    },
    {
      path: 'clients-update',
      component: ClientsUpdateComponent
    }
   ]
 }

];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ClientsRoutingModule { }

import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CommentsCreateComponent } from './comments-create/comments-create.component';
import { CommentsListComponent } from './comments-list/comments-list.component';
import { MainCommentsComponent } from './main-comments/main-comments.component';


const routes: Routes = [
  {
    path:'',
    component: MainCommentsComponent,
    children: [
      {
        path: '',
        component: CommentsListComponent
      },
      {
        path: 'comments-create',
        component: CommentsCreateComponent
      }
      
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CommentsRoutingModule { }

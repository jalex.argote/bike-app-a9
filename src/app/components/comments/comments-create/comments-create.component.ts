import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import { IComments } from '../interfaces/comments';
import { CommentsService } from '../comments.service';

@Component({
  selector: 'app-comments-create',
  templateUrl: './comments-create.component.html',
  styleUrls: ['./comments-create.component.styl']
})
export class CommentsCreateComponent implements OnInit {
  commentsFormGroup: FormGroup;
  
  comments: IComments[];
  
  statusSearchPostId: boolean = true;

  formSearchComment = this.formBuilder.group({
    postId: ['']
  })

  constructor(private formBuilder: FormBuilder, private commentsService: CommentsService) {}
  
  ngOnInit() {
  }

  searchComments() {
    console.warn('Data', this.formSearchComment.value);
    this.commentsService.getCommentsByPostId(this.formSearchComment.value.postId)
    .subscribe(res => {
      
      if(res.length > 0){
        this.comments = res;
        this.statusSearchPostId = true;
        console.warn('Response comments by postId', res);
      } else {
        this.statusSearchPostId = false;
        console.warn('No encontrado');
        this.comments = [];
      }
      //this.comments = res;
      //this.statusSearchPostId = false;
    });/*, error => {
      console.warn('No encontrado', error);
      this.comments = null;
      this.statusSearchPostId = true;
    });*/
  }

}

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CommentsCreateComponent } from './comments-create/comments-create.component';
import { CommentsListComponent } from './comments-list/comments-list.component';
import { CommentsRoutingModule } from './comments-routing.module';
import { ReactiveFormsModule } from '@angular/forms';
import { MainCommentsComponent } from './main-comments/main-comments.component';



@NgModule({
  declarations: [
    CommentsCreateComponent, 
    CommentsListComponent, 
    MainCommentsComponent
  ],
  imports: [
    CommonModule,
    CommentsRoutingModule,
    ReactiveFormsModule
  ]
})
export class CommentsModule { }

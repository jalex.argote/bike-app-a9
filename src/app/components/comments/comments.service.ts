import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { IComments } from './interfaces/comments';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class CommentsService {

  constructor(private http: HttpClient) { }

  public query(): Observable<IComments[]> {
    return this.http.get<IComments[]>(`${environment.END_POINT_JSON}/comments`)
      .pipe(map(res => {
        return res;
      }))
  }

  public saveComments(comments: IComments): Observable<IComments> {
    return this.http.post<IComments>(`${environment.END_POINT_JSON}/comments`, comments)
    .pipe(map(res => {
      return res;
    }));
  } 

  public getCommentsByPostId(postId: string): Observable<IComments[]>{
    let params = new HttpParams();
    params = params.append('postId', postId);
    console.warn('Params', params);
    return this.http.get<IComments[]>(`${environment.END_POINT_JSON}/comments`, {params: params})
      .pipe(map(res => {
        return res;
      }));
  }
}

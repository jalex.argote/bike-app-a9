import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from './auth/login/login.component';
import { Authority } from './auth/auth-shared/constants/authority.constants';
import { UserRouteAccessGuard } from './auth/guards/user-route-access.guard';
import { AccessDeniedComponent } from './auth/access-denied/access-denied.component';


const routes: Routes = [
  {
    path: 'login',
    component: LoginComponent
  },
  {
    path: '',
    redirectTo: 'login',
    pathMatch: 'full'
  },
  {
    path: 'dashboard',
    data: {
      authorities: [Authority.USER, Authority.ADMIN]
    },
    canActivate: [UserRouteAccessGuard],
    loadChildren: () => import('../app/dashboard/dashboard.module')
      .then(modulo => modulo.DashboardModule)
  },
  {
    path: 'access-denied',
    component: AccessDeniedComponent
  },
  {
    path: 'bikes',
    loadChildren: () => import('./components/bikes/bikes.module')
      .then(modulo => modulo.BikesModule)
  },
  {
    path: 'admin',
    loadChildren: () => import('./components/admin/admin.module')
      .then(modulo => modulo.AdminModule)
  },
  {
    path: 'clients',
    loadChildren: () => import('./components/clients/clients.module')
      .then(modulo => modulo.ClientsModule)
  },
  {
    path: 'sales',
    loadChildren: () => import('./components/sales/sales.module')
      .then(modulo => modulo.SalesModule)
  },
  {
    path: 'comments',
    loadChildren: () => import('./components/comments/comments.module')
      .then(modulo => modulo.CommentsModule)
  },
  {
    path: 'posts',
    loadChildren: () => import('./components/posts/posts.module')
      .then(modulo => modulo.PostsModule)
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }

import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { ICredentials, Credentials } from '../auth-shared/models/credentials';
import { LoginService } from '../login/login.service'
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.styl']
})
export class LoginComponent implements OnInit {
  loginForm = this.fb.group({
    username: ['', Validators.compose([
      Validators.required])],
    password: ['', Validators.compose([
      Validators.required])],
    rememberMe: false
  });


  constructor(
    private fb: FormBuilder,
    private loginService: LoginService,
    // private router: Router,
  ) { }

  ngOnInit(): void {
  }

  login(): void {
    const credentials: ICredentials = new Credentials()
    credentials.username = this.loginForm.value.username;
    credentials.password = this.loginForm.value.password;
    credentials.rememberMe = this.loginForm.value.rememberMe

    this.loginService.login(credentials)
      .subscribe((res: any) => {
        console.warn('DATA RESPONSE CONTROLLER ', res);
        // this.router.navigate(['/dashboard']);

      }, (error: any) => {
        console.warn('ERROR ', error)
        if (error.status === 401) {
          window.alert('Usuario o contraseņa incorrectos');
        }
      });
  }
}

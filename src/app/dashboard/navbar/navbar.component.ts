import { Component, OnInit } from '@angular/core';
import { faSearch } from '@fortawesome/free-solid-svg-icons';
import { faBars } from "@fortawesome/free-solid-svg-icons";
import { Router } from '@angular/router';
import { AccountService } from 'src/app/auth/account.service';
import { LoginService } from 'src/app/auth/login/login.service';


@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.styl']
})
export class NavbarComponent implements OnInit {

  nameUser: string;
  constructor(
    private loginService: LoginService,
    private router: Router,
    private accountService: AccountService
  ) { }

  ngOnInit(): void {
    this.getName();
  }

  getName(): void {
    this.nameUser = this.accountService.getUserName();
  }
  /*
    faSearch = faSearch;
    faBars = faBars;
    */
  logout(): void {
    this.loginService.logout()
    this.router.navigate(['/login'])
  }
}

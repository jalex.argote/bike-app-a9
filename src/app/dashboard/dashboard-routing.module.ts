import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { MainDashboardComponent } from './main-dashboard/main-dashboard.component';
import { MainBikesComponent } from '../components/bikes/main-bikes/main-bikes.component';
import { BikesCreateComponent } from '../components/bikes/bikes-create/bikes-create.component';
import { BikesListComponent } from '../components/bikes/bikes-list/bikes-list.component';
import { BikesUpdateComponent } from '../components/bikes/bikes-update/bikes-update.component';
import { BokesViewComponent } from '../components/bikes/bokes-view/bokes-view.component';
import { MainClientsComponent } from '../components/clients/main-clients/main-clients.component';
import { ClientsCreateComponent } from '../components/clients/clients-create/clients-create.component';
import { ClientsListComponent } from '../components/clients/clients-list/clients-list.component';
import { ClientsUpdateComponent } from '../components/clients/clients-update/clients-update.component';
import { MainSalesComponent } from '../components/sales/main-sales/main-sales.component';
import { SaleCreateComponent } from '../components/sales/sale-create/sale-create.component';
import { SaleListComponent } from '../components/sales/sale-list/sale-list.component';
import { Authority } from '../auth/auth-shared/constants/authority.constants';
import { UserRouteAccessGuard } from '../auth/guards/user-route-access.guard';


const routes: Routes = [
  {
    path: '',
    component: MainDashboardComponent,
    children: [{
      /* Inicio rutas hijas Bike*/
      path: 'bike',
      data: {
        authorities: [Authority.ADMIN]
      },
      canActivate: [UserRouteAccessGuard],
      component: MainBikesComponent,
      children: [{
        path: 'bike-create',
        component: BikesCreateComponent
      },
      {
        path: 'bike-list',
        component: BikesListComponent
      },
      {
        path: 'bike-update\:id',
        component: BikesUpdateComponent
      },
      {
        path: 'bike-view',
        component: BokesViewComponent
      }
      ]
    },
    /*Fin rutas hijas Bike*/
    /*Inicio rutas hijas Client*/
    {
      path: 'client',
      data: {
        authorities: [Authority.ADMIN]
      },
      canActivate: [UserRouteAccessGuard],
      component: MainClientsComponent,
      children: [{
        path: 'clients-create',
        component: ClientsCreateComponent
      },
      {
        path: 'clients-list',
        component: ClientsListComponent
      },
      {
        path: 'clients-update',
        component: ClientsUpdateComponent
      }
      ]

    },
    /* Fin rutas hijas Client*/
    /*Inicio rutas hijas Sale*/
    {
      path: 'sale',
      data: {
        authorities: [Authority.USER, Authority.ADMIN, Authority.CLIENT]
      },
      canActivate: [UserRouteAccessGuard],
      component: MainSalesComponent,
      children: [{
        path: 'sale-create',
        component: SaleCreateComponent
      },
      {
        path: 'sale-list',
        component: SaleListComponent
      }
      ]
    }
      /*Fin rutas hijas Sale*/
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DashboardRoutingModule { }

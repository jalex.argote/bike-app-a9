import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { DashboardRoutingModule } from './dashboard-routing.module';
import { NavbarComponent } from './navbar/navbar.component';
import { MainDashboardComponent } from './main-dashboard/main-dashboard.component';
import { FooterComponent } from './footer/footer.component';
import { SlideComponent } from './slide/slide.component';
import { FaIconLibrary } from '@fortawesome/angular-fontawesome';
import { faSearch, faBars } from '@fortawesome/free-solid-svg-icons';
import { SharedAuthModule } from '../auth/auth-shared/auth-shared.module';


@NgModule({
  declarations: [NavbarComponent, MainDashboardComponent, FooterComponent, SlideComponent],
  imports: [
    CommonModule,
    DashboardRoutingModule,
    SharedAuthModule
  ]
})
export class DashboardModule {
  constructor(Library: FaIconLibrary) {
    Library.addIcons(faSearch, faBars)
  }
}
